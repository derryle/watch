#!/bin/bash
#title           :log_watch.sh
#description     :This script will check specific directories for removal of log files
#author		 :Iram Hussain
#date            :20180418
#version         :1.0
#usage		 :bash log_watch.sh
#notes           :Uses Linux auditd, aureport, ausearch 
#==============================================================================

# Set the directory to monitor

LOGDIR=/apps/SAS/v9.4/config/Lev1/SASMeta/MetadataServer/Logs
LOGDIR2=/apps/SAS/v9.4/config/Lev1/SASMeta/MetadataServer/AuditLogs

# Remove rules for logs that are older/have not been modified in 30 days.

for i in `find $LOGDIR/*.log -mtime +30`
do
    auditctl -W $i -p rwxa -k $i-rule 2> /dev/null
done

# Determine which logs are less than or equal to 30 days,
# run the results in a loop, and create individual audit rules.
# This creates a database of entries using Linux auditd.

for i in `find $LOGDIR/*.log -mtime -30`
do
    auditctl -w $i -k $i-rule 2> /dev/null
done

for i in `find $LOGDIR2/*.log -mtime -30`
do
    auditctl -w $i -k $i-rule 2>/dev/null
done

# Periodically check the audit log for any removed log files
# The search timeframe is a 10 minute window so there are no
# duplicate or false alarms.

function_check() {

for i in `auditctl -l |grep SAS|awk '{print $6}'`
do
    ausearch -k $i |aureport  -f -ts recent -te today |egrep "rm|mv"
done

}

# Output the results, check this file "alert.out"

function_check  |awk '{print $2" "$3" DELETED: "$4}' | sort > alert.out